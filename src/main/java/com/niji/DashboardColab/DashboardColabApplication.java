package com.niji.DashboardColab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DashboardColabApplication {

	public static void main(String[] args) {
		SpringApplication.run(DashboardColabApplication.class, args);
	}

}
